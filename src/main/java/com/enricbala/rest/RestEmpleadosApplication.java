package com.enricbala.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RestEmpleadosApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RestEmpleadosApplication.class, args);
	}

}
